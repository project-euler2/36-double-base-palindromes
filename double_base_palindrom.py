import math
import time
start_time = time.time()

def find_double_base_palindrom(limit):
    double_palindroms = []
    for i in range(1,limit,2):
        if str(i) == str(i)[::-1]:
            if str(bin(i))[2:] == str(bin(i))[2:][::-1]:
                double_palindroms.append(i)
    return sum(double_palindroms)

print(find_double_base_palindrom(1000000))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )